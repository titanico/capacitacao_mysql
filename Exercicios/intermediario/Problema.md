# Problema

A Politécnica necessita de um sistema para controle de entrada de veículos no estacionamento.

## Funcionamento

1. **Registro**: A pessoa com o veículo interessada em estacionar(apenas
professor e funcionário) deve comunicar previamente ao
Secretário para que o mesmo adicione:
    a. O nome, cpf do professor/funcionário numa planilha;

    b. A placa, cor, modelo do veículo na mesma planilha;

    c. A data de início da autorização para o estacionamento;

    d. A data de fim da autorização para o estacionamento;

    e. A motivação/justificativa para a concessão da autorização: um texto curto e direto que explique o motivo;
2. **Verificação**: O porteiro, para quaisquer indivíduos que requisitem acesso ao estacionamento da instituição, deve:

    a. Perguntar o nome e cpf da pessoa;

    b. Verificar a placa, cor e/ou modelo do veículo;

    Caso exista na planilha alguma entrada cujas informações condizem com os itens a. e b. fornecidos, o porteiro permitirá a entrada do veiculo e o motorista,
    caso contrário, não a permitirá.

3. **Autenticação**: É importante que para _1. Registro_ haja uma segurança que permita que apenas Secretários selecionados pela instituição acessem a aplicação. Em _2. Verificação_ não há necessidade de tal segurança.

## Tarefa

1. Crie uma base dados em MySQL;

2. Modele as tabelas e suas relações de maneira que contemplem as situações do problema descrito. O banco, posteriormente, será utilizado por uma aplicação que consumirá os dados registrados no mesmo;

3. Codifique um _script_ em SQL que crie as tabelas e defina suas relações. Lembre da sintaxe para criação de tabelas com chave estrangeira;

4. Popule a tabela com cargas de teste e pratique consultas utilizando INNER JOIN que relacionem informações de diferentes tabelas através da chave estrangeira das mesmas.
