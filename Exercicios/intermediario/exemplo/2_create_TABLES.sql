USE parkingdb_[SEU NOME];

CREATE TABLE IF NOT EXISTS PES_PESSOA(
    pes_id_pessoa INT NOT NULL AUTO_INCREMENT,
    pes_nm_pessoa VARCHAR(50) NOT NULL,
    pes_nu_cpf VARCHAR(11) NOT NULL,
    
    PRIMARY KEY (pes_id_pessoa)
);

CREATE TABLE IF NOT EXISTS TIU_TIPO_USUARIO(
    tiu_id_tipo_usuario INT NOT NULL AUTO_INCREMENT,
    tiu_ds_cargo VARCHAR(20) NULL,
    tiu_ch_administrador TINYINT(1) NOT NULL,
    
    PRIMARY KEY(tiu_id_tipo_usuario)
);

CREATE TABLE IF NOT EXISTS USR_USUARIO(
    usr_id_usuario INT NOT NULL AUTO_INCREMENT,
    usr_id_pessoa INT NOT NULL REFERENCES PES_PESSOA(pes_id_pessoa),
    usr_id_tipo_usuario INT NOT NULL REFERENCES TIU_TIPO_USUARIO(tiu_id_tipo_usuario),
    usr_ds_nickname VARCHAR(15) NOT NULL,
    usr_ds_senha VARCHAR(255) NOT NULL,

    PRIMARY KEY(usr_id_usuario)
);

CREATE TABLE IF NOT EXISTS VEC_VEICULO(
    vec_id_veiculo INT NOT NULL AUTO_INCREMENT,
    vec_ds_cor VARCHAR(15) NULL,
    vec_ds_modelo VARCHAR(15) NULL,
    vec_nu_placa VARCHAR(8) NULL,

    PRIMARY KEY(vec_id_veiculo)
);

CREATE TABLE IF NOT EXISTS AUT_AUTORIZACAO(
    aut_id_autorizacao INT NOT NULL AUTO_INCREMENT,
    aut_id_pessoa INT NOT NULL REFERENCES PES_PESSOA(pes_id_pessoa),
    aut_id_veiculo INT NULL REFERENCES VEC_VEICULO(vec_id_veiculo),
    aut_ds_autorizacao VARCHAR(255),
    aut_dt_cadastro DATE NOT NULL,
    aut_dt_expiracao DATE NOT NULL,

    PRIMARY KEY(aut_id_autorizacao)
);
