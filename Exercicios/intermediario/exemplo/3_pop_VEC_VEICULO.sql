USE parkingdb_[SEU NOME];

INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (1, 'Vermelho', 'Cherokee', 'PVNA6613');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (2, 'Azul', 'Forester', 'XWBS7424');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (3, 'Branco', 'Ridgeline', 'EGMJ1137');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (4, 'Amarelo', 'MKS', 'YXBU7659');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (5, 'Abrilhantado', 'Sunbird', 'VOXS4746');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (6, 'Pau-brasil', 'Q5', 'PYQA6743');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (7, 'Branco', 'Grand Vitara', 'YBNI7889');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (8, 'Lilás', 'CL-Class', 'FXVS0416');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (9, 'Azul', 'C70', 'KAZF5825');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (10, 'Lilás', '9-2X', 'IJXG6550');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (11, 'Branco', 'VUE', 'UGXW1454');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (12, 'Azul', '525', 'OUSJ8619');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (13, 'Lilás', 'RX', 'JLTC5525');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (14, 'Pau-brasil', 'Corvair', 'YOAH2854');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (15, 'Azul', 'Mustang', 'DVZJ1048');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (16, 'Roxo', 'Civic', 'ZQSX3992');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (17, 'Abóbora', '300CE', 'FBAG5198');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (18, 'Lilás', 'E-Class', 'JIWH8824');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (19, 'Violeta', 'Supra', 'NZUA5441');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (20, 'Azul', 'TT', 'LDJM4191');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (21, 'Lilás', 'Sable', 'PBTO7558');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (22, 'Azul', 'Pathfinder', 'LVYT4119');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (23, 'Amarelo', 'ES', 'MRTC0332');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (24, 'Vermelho', 'Justy', 'OFWP8919');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (25, 'Azul', 'Express 2500', 'PTVS7565');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (26, 'Vermelho', 'S40', 'XQHA1705');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (27, 'Vermelho', 'DeVille', 'NFAK8496');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (28, 'Ciano', 'Talon', 'PSON1756');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (29, 'Branco', 'Enclave', 'SCLK2017');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (30, 'Vermelho', '200SX', 'OZFM9447');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (31, 'Amarelo', '240SX', 'LHXG6150');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (32, 'Azul', 'GT-R', 'UXDJ0469');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (33, 'Ciano', 'B-Series', 'QMHF0174');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (34, 'Vermelho', 'Suburban 1500', 'YFVQ7693');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (35, 'Ciano', 'Grand Caravan', 'KNMB6582');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (36, 'Azul', 'Grand Prix', 'HKOR1799');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (37, 'Branco', 'Biturbo', 'HQXU2781');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (38, 'Cor de mijo', '3500', 'YFHT2977');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (39, 'Amarelo', 'Matrix', 'NHID8060');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (40, 'Azul', 'Starion', 'FEZJ9720');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (41, 'Cinza', 'Grand Vitara', 'VGCU6874');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (42, 'Ciano', '4Runner', 'HJVP2516');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (43, 'Azul', 'A8', 'RSLD6101');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (44, 'Cinza', 'Suburban 2500', 'ZLES8584');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (45, 'Magenta', 'Grand Prix', 'IJEM8573');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (46, 'Rosa', '3500', 'RWLH9693');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (47, 'Azul', '9-5', 'TLOI0789');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (48, 'Amarelo', 'Mirage', 'VTKO2623');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (49, 'Cinza', 'Coupe Quattro', 'TGMU5277');
INSERT INTO VEC_VEICULO (vec_id_veiculo, vec_ds_cor, vec_ds_modelo, vec_nu_placa) values (50, 'Cor de mijo', 'G35', 'AOIS2551');
